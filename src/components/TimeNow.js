import { Component } from "react";

class TimeNow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            color: "black"
        }
    }

    onButtonClickHandler = () => {
        const seconds = new Date().getSeconds();

        if(seconds % 2 === 0)
            this.setState({
                color: "red"
            })
            else {
                this.setState({
                    color: "blue"
                })
            }
        
    }

    render() {
        let today = new Date().toLocaleTimeString();
        return (
            <div style={{color: this.state.color}}>
                <h1>Hello, world!</h1>
                <h3>It is {today}</h3>
                <button onClick={this.onButtonClickHandler}>Change Color</button>
            </div>
        )
    }
}

export default TimeNow;